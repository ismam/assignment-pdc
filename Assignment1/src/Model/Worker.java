package Model;

import Utility.DocumentReadingObserver;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Worker extends Thread{

    private final MainMonitor monitor;
    private final String id;
    private final String path;
    private final DocumentReadingObserver observer;
    private volatile boolean isPaused = false;
    private final Object pauseLock = new Object();
    private final String[] excludedWords;

    public Worker(MainMonitor monitor, String id, String path, DocumentReadingObserver ob, String[] excludedWords){
        this.monitor = monitor;
        this.id = id;
        this.path = path;
        this.observer = ob;
        this.excludedWords = excludedWords;
    }

    public void run() {
        while(monitor.getSize()>0){
            this.pauseResume();
            String name = monitor.getWork();
            try {
                this.compute(name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void pauseThreads(){
        this.isPaused = true;
    }

    public void resumeThreads(){
        synchronized (pauseLock) {
            this.isPaused = false;
            pauseLock.notifyAll(); // Unblocks thread
        }
    }

    private void pauseResume(){
        if(isPaused){
            synchronized (pauseLock){
                try {
                    pauseLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void compute(String fileName) throws IOException {

        PDDocument document = PDDocument.load(new File(path + fileName));
        AccessPermission ap = document.getCurrentAccessPermission();
        this.checkAccessPermission(ap);
        this.count(document, fileName);
        document.close();
    }

    private void count(PDDocument document, String fileName) throws IOException {

        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);
        for (int p = 1; p <= document.getNumberOfPages(); ++p) {
            stripper.setStartPage(p);
            stripper.setEndPage(p);
            String text = stripper.getText(document);

            text = text.toLowerCase();
            String[] words = text.trim().split("\\W+");
            this.addToMap(this.compareWords(this.excludedWords, words));
        }
        observer.notifyReadingCompleted(fileName, this.id);
        observer.notifyMessage("In the map there are " + monitor.getTotalProcessedWords() + " words");
    }

    private String[] compareWords(String[] filter, String[] words){

        List<String> listWords = Arrays.stream(words).collect(Collectors.toList());
        List<String>  listFilters = Arrays.stream(filter).collect(Collectors.toList());
        listFilters.forEach(v -> listWords.removeIf(s -> s.contains(v)));
        return listWords.toArray(new String[0]);
    }

    private void addToMap(String[] words){
        int totalWords =0;
        for (String s : words){
            monitor.addMap(s);
            totalWords++;
        }
        monitor.incrementWordCounter(totalWords);
    }

    private void checkAccessPermission(AccessPermission ap){
        if (!ap.canExtractContent()){
            try {
                throw new IOException("You do not have permission to extract text");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
