package View.Listerner;

import Controller.GuiController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StopListener implements ActionListener {
    private GuiController controller;

    public StopListener(GuiController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Stop computation but not exist from gui
        Thread th = new Thread(() -> {
            controller.stop();
        });
        th.start();
    }
}
