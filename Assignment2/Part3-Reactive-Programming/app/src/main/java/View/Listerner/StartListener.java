package View.Listerner;

import Controller.GuiController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class StartListener implements ActionListener {
    private GuiController controller;
    private final JTextField dirPath;
    private final JTextField exclusionFilePath;
    private final JTextField numberOfOccurrences;
    private String path;
    private String exclusion;
    private int occurrences;

    public StartListener(GuiController controller, JTextField dirPath, JTextField exclusionFile, JTextField numberOfOccurrences) {
        this.controller = controller;
        this.dirPath = dirPath;
        this.exclusionFilePath = exclusionFile;
        this.numberOfOccurrences = numberOfOccurrences;
    }
    @Override
    public void actionPerformed(ActionEvent e) {

        path = dirPath.getText().trim();
        exclusion = exclusionFilePath.getText().trim();
        occurrences = this.numberOfOccurrences.getText().isEmpty() || !this.numberOfOccurrences.getText().chars().allMatch(Character::isDigit) ?  -1 : Integer.parseInt(this.numberOfOccurrences.getText());

        if(path.isBlank() || !new File(path).isDirectory()) {
            JOptionPane.showMessageDialog(null, "Please enter a correct directory path");
            return;
        }

        if( !exclusion.isBlank() && !exclusion.endsWith(".txt") ){
            JOptionPane.showMessageDialog(null, "Please enter a correct exclusion file name");
            return;
        }

        if(occurrences < 0) {
            JOptionPane.showMessageDialog(null, "Please enter a correct number of occurrences");
            return;
        } else {
            try {
                occurrences = Integer.parseInt(numberOfOccurrences.getText().trim());
            } catch (NumberFormatException ex ) {
                JOptionPane.showMessageDialog(null, "Please enter a correct value");
                //We are on view, no need to print stacktrace
            }
        }

        Thread th = new Thread(() -> {
            path = path + "/";
            //Setup
            controller.setup();

            //Start computation
            controller.launch(path, exclusion, occurrences);
        });
        th.start();
    }
}
