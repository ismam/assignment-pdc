package View;

import Controller.GuiController;
import Logger.CustomPrintStream;
import Logger.GuiOutPutStream;
import View.Listerner.StartListener;
import View.Listerner.StopListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.PrintStream;

public class View  {

    private JFrame frame;
    private JButton start;
    private JButton stop;
    private JLabel pdfPath;
    private JLabel exclusionFile;
    private JLabel occurrence;
    private JScrollPane scrollPane;
    private JTextField dirPath;
    private JTextField exclusionFilePath;
    private JTextField numberOfOccurrences;
    private JTextArea txtArea;
    private JPanel panel;
    private GuiController controller;
    private GuiOutPutStream output;


    public void prepareGui(GuiController controller) {
        this.controller = controller;
        this.panel = new JPanel();
        frame = new JFrame("First Assignment");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,700);

        start = new JButton("Start");
        stop = new JButton("stop");

        txtArea = new JTextArea(30,50);
        exclusionFilePath = new JTextField(15);
        numberOfOccurrences = new JTextField(15);
        dirPath = new JTextField(15);
        txtArea.setEditable(false);

        scrollPane = new JScrollPane(txtArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pdfPath = new JLabel("Insert directory that contain files");
        exclusionFile = new JLabel("Insert path of the exclusion file");
        occurrence = new JLabel("Insert number of occurrences");

        panel.setBorder(new EmptyBorder(new Insets(50, 50, 100, 50)));

        panel.add(exclusionFile);
        panel.add(exclusionFilePath);

        panel.add(occurrence);
        panel.add(numberOfOccurrences);

        panel.add(pdfPath);
        panel.add(dirPath);

        panel.add(scrollPane);

        panel.add(stop);
        panel.add(start);
        start.addActionListener(new StartListener(controller, dirPath, exclusionFilePath, numberOfOccurrences));
        stop.addActionListener(new StopListener(controller));
        //Setup system out to print on textArea
        output = new GuiOutPutStream(txtArea);
        System.setOut(new PrintStream(output, true));
        System.setOut(new CustomPrintStream(System.out));

        frame.getContentPane().add(panel);

        //make frame visible
        frame.setVisible(true);
    }
}

