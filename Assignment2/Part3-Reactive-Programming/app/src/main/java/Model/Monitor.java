package Model;

import java.util.*;
import java.util.stream.IntStream;

public class Monitor {

    private final Map<String, Integer> words = new HashMap<>();
    private int occurrences = 5;

    public synchronized void addElem(String key){
        words.put(key, words.containsKey(key) ? words.get(key)+1 : 1);
    }

    public void setOccurrences(int occurrences){
        this.occurrences = occurrences;
    }

    public synchronized void printMap(){
        //words.keySet().forEach(e -> System.out.println("Key: " + e + " - Value: " + words.get(e)));
        List<String> lis = new ArrayList<>();
        IntStream.range(0, this.occurrences).boxed().forEach(v->{
            String key = "";
            int value = 0;
            for(Map.Entry<String, Integer> entry : words.entrySet()){
                if(entry.getValue()> value){
                    key = entry.getKey();
                    value = entry.getValue();
                }
            }
            lis.add("The word '" + key + "' is used " + value + " times.");
            words.remove(key);
        });
        lis.forEach(System.out::println);
    }
}
