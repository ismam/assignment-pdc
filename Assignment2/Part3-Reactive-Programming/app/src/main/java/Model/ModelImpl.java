package Model;

import Model.Tasks.DocumentLoaderTask;
import Model.Tasks.DocumentSplitterTask;
import Model.Tasks.PageWordCounter;
import Model.Tasks.SetFileNamesTask;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ModelImpl implements Model {
    private Logger log = Logger.getLogger(ModelImpl.class.getName());
    private ExecutorService svc;
    private Monitor monitor;

    public ModelImpl(ExecutorService svc, Monitor monitor) {
        this.svc = svc;
        this.monitor = monitor;
    }

    @Override
    public void startComputation(String directoryPath, String excludedFilePath, int occurrences) throws IOException, InterruptedException {

        Set<String> excludedWords = this.setExcludedFile(excludedFilePath);
        monitor.setOccurrences(occurrences);
        System.out.println("OCCURRENCES: "+occurrences);
        try {
            Schedulers.start();

            Observable.just(new SetFileNamesTask().apply(directoryPath))
                    .observeOn(Schedulers.io())
                    .flatMap(f -> Observable.fromStream(f.stream()))
                    .map(e -> new DocumentLoaderTask().apply(e))
                    .observeOn(Schedulers.computation())
                    .map(r -> new DocumentSplitterTask().apply(r))
                    .observeOn(Schedulers.from(svc))
                    .flatMap(g -> Observable.fromStream(g.stream()))
                    .map(t -> new PageWordCounter(monitor, excludedWords).apply(t))
                    .blockingSubscribe();
        } finally {
            Schedulers.shutdown();
            svc.shutdown();
            svc.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        }
    }

    @Override
    public void stop() {
        Schedulers.shutdown();
        svc.shutdown();
        try {
            if (!svc.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                svc.shutdownNow();
            }
            System.out.println("Stopped all computation");
        } catch (InterruptedException e) {
            svc.shutdownNow();
            System.out.println("Stopped all computation");
        }
    }

    private Set<String> setExcludedFile(String excludedFilePath) throws IOException {
        return new HashSet<>(Files.readAllLines(Path.of(excludedFilePath)));
    }

    private static void log(String msg) {
        System.out.println("[" + Thread.currentThread().getName() + "] " + msg);
    }

}
