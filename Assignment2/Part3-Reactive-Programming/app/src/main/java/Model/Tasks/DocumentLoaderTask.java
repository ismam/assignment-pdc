package Model.Tasks;

import Model.Monitor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;

public class DocumentLoaderTask implements Function<String, PDDocument> {

    @Override
    public PDDocument apply(String path) {
        try {
            PDDocument document = PDDocument.load(new File(path));
            AccessPermission ap = document.getCurrentAccessPermission();
            this.checkAccessPermission(ap);
            System.out.println(Thread.currentThread().getName() + ": Load document");
            return document;
        } catch (IOException e) {
            System.out.println("Something went wrong, please check " + e.getMessage());
            return null;
        }
    }

    private void checkAccessPermission(AccessPermission ap){
        if (!ap.canExtractContent()){
            try {
                throw new IOException("You do not have permission to extract text");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
