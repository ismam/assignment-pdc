package Model.Tasks;

import Model.Monitor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class DocumentSplitterTask implements Function<PDDocument, List<String>> {

    @Override
    public List<String> apply(PDDocument pdDocument) {

        try {
            List<String> pages = new ArrayList<>();
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(true);
            for (int p = 1; p <= pdDocument.getNumberOfPages(); ++p) {
                stripper.setStartPage(p);
                stripper.setEndPage(p);
                pages.add(stripper.getText(pdDocument).toLowerCase());
            }
            pdDocument.close();
            System.out.println(Thread.currentThread().getName() + " :Split document ");
            return pages;

        } catch (IOException e) {
            System.out.println("Some went wrong, please check " + e.getMessage());
            return null;
        }
    }
}
