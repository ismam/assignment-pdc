package Model.Tasks;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SetFileNamesTask implements Function<String, List<String>> {

    @Override
    public List<String> apply(String dirName) {

        File folder = new File(dirName); //"D:" + File.separator + "PCD" + File.separator  + "PDF" + File.separator
        File[] listOfFiles = folder.listFiles();
        assert listOfFiles != null;

        return Arrays.stream(listOfFiles)
                .filter(File::isFile)
                .map(File::getName)
                .filter(name -> name.endsWith(".pdf"))
                .map(e -> dirName+e)
                .collect(Collectors.toList());
    }
}
