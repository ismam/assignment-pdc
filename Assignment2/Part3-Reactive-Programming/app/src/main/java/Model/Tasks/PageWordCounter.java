package Model.Tasks;

import Model.Monitor;

import java.util.*;
import java.util.function.Function;

public class PageWordCounter implements Function<String,String> {

    Monitor monitor;
    private Object o = new Object();
    Set<String> excludedWordsList;

    public PageWordCounter(Monitor monitor, Set<String> excludedWordsList){
        this.monitor = monitor;
        this.excludedWordsList = excludedWordsList;
    }

    @Override
    public String apply(String s) {
        Arrays.stream(s.trim().split("\\W+")).filter( e -> !excludedWordsList.contains(e)).forEach(monitor::addElem);
        System.out.println(Thread.currentThread().getName() + ": Added new page");
        return s;
    }

}
