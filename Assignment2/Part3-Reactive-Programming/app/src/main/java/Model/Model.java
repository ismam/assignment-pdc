package Model;

import java.io.IOException;

public interface Model {

    void startComputation(String directoryPath, String excludedFilePath, int occurrences) throws IOException, InterruptedException;

    void stop();

}
