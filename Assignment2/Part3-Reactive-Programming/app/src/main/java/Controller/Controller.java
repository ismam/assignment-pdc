package Controller;

import java.io.IOException;

public interface Controller {

    void init();

    void launch(String directoryPath, String excludedFilePath, int occurrences) throws IOException;
}
