package Controller;

import Model.Model;
import Model.ModelImpl;
import Model.*;
import View.View;

import java.io.IOException;
import java.util.concurrent.*;

public class GuiController implements Controller {

    private Model model;
    private Monitor monitor;
    private ExecutorService svc;
    private int poolSize;

    public GuiController() {

    }

    public void setup() {
        //pool size
        poolSize = Runtime.getRuntime().availableProcessors() + 1;
        svc = new ThreadPoolExecutor(poolSize, poolSize, 0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),
                new ThreadPoolExecutor.DiscardPolicy());
        this.monitor = new Monitor();
        this.model = new ModelImpl(svc, monitor);
    }

    @Override
    public void init() {
        new View().prepareGui(this);
    }

    @Override
    public void launch(String directoryPath, String excludedFilePath, int occurrences) {
        try {
            model.startComputation(directoryPath, excludedFilePath, occurrences);
            monitor.printMap();
        } catch (IOException | InterruptedException e) {
            System.out.println("Something went wrong, please check " + e.getMessage());
        }
    }

    public void stop() {
        model.stop();
    }
}
