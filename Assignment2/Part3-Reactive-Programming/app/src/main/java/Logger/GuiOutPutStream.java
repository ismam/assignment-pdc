package Logger;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;

public class GuiOutPutStream extends OutputStream {

    JTextArea textArea;

    public GuiOutPutStream(JTextArea textArea){
        this.textArea = textArea;
    }

    @Override
    public void write(int data) throws IOException {
        SwingUtilities.invokeLater (new Runnable () {
            public void run() {
                textArea.append(new String(new byte[]{(byte) data}));
            }
        });
    }
}
