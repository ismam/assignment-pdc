package Utility;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Functions {

    public static List<List<String>> applyFilter(String res){
        return atomicSplit(stringCleaner(res));
    }

    public static void printTableOfTrain(List<List<String>> res){
        IntStream.range(0, res.get(0).size()).boxed().forEach(e ->{
            System.out.println("Solution number: " + (e));
            System.out.println("Solution: " + res.get(0).get(e));
            System.out.println("Departure time: " + res.get(1).get(e));
            System.out.println("Arrival time: " + res.get(2).get(e));
            System.out.println("Duration: " + res.get(3).get(e));
            System.out.println("Changes: " + res.get(4).get(e));
            System.out.println("Train Identifier: " + res.get(5).get(e));
            System.out.println("\n");
        });
    }

    public static void printTableOfTrainRaw(List<List<String>> res){
        System.out.println(res.toString());
    }

    public static List<String> filterTrainInfo(List<String> info){
        return info.stream().map(e -> e.replace("<p>","")
                .replace("<br>", "")
                .replace("<strong>", "")
                .replace("</strong>","")
                .replace("</p>",""))
                .collect(Collectors.toList());
    }

    public static List<String> filterStationInfo(List<String> info){
        return info.stream().map(e -> e.replace("<p>","")
                .replace("<br>", "")
                .replace("<strong>", "")
                .replace("</strong>","")
                .replace("</p>","")
                .replace("<h2>", "")
                .replace("</h2>","")
                .replace("<div class=\"bloccotreno\">", "")
                .replace("<img src=\"../images/pallinoRit0.png\">","")
                .replace("<img src=\"../images/pallinoRit1.png\">","")
                .replace("</div>", "")
                .replace("<a href=\"scheda?", "")
                .replace("lang=IT\">", "")
                .replace("Vedi scheda</a>", "")
                .replaceAll("\n[ \t]*\n", "\n"))
                .collect(Collectors.toList());
    }

    public static List<List<String>> splitStations(List<String> res){
        List<String> trainName = new ArrayList<>();
        List<String> destination = new ArrayList<>();
        List<String> time = new ArrayList<>();
        List<String> expectedBinary = new ArrayList<>();
        List<String> realBinary = new ArrayList<>();
        List<String> delayTime = new ArrayList<>();
        List<String> info = new ArrayList<>();

        List<List<String>> sublist = new ArrayList<>();

        res.forEach(e -> sublist.add(Arrays.asList(e.split("\n"))));
        sublist.remove(0);

        sublist.forEach(e -> {
            trainName.add(e.get(1));
            destination.add(e.get(3));
            time.add(e.get(5));
            expectedBinary.add(e.get(6));
            realBinary.add(e.get(7));
            delayTime.add(e.get(8));
            info.add(e.get(9));
        });

        List<List<String>> a = new ArrayList<>();
        a.add(trainName);
        a.add(destination);
        a.add(time);
        a.add(expectedBinary);
        a.add(realBinary);
        a.add(delayTime);
        a.add(info);
        return a;
    }

    private static List<String> stringCleaner(String res){
        String[] lis = res.split("(?<=},)");
        return Arrays.stream(lis).map(s -> s.replace("[", "")
                .replace("]", "")
                .replace("{", "")
                .replace("}", ""))
                .collect(Collectors.toList());
    }

    private static List<List<String>> atomicSplit(List<String> res){
        List<String> idSolutions = new ArrayList<>();
        List<String> departureTimes  = new ArrayList<>();
        List<String> arrivalTimes  = new ArrayList<>();
        List<String>durations  = new ArrayList<>();
        List<String> changes  = new ArrayList<>();
        List<String> trainIdentifiers  = new ArrayList<>();

        List<List<String>> sublist = new ArrayList<>();

        res.forEach(e -> sublist.add(Arrays.asList(e.split(","))));

        sublist.forEach(e -> {
            idSolutions.add(e.get(0));
            departureTimes.add(e.get(4));
            arrivalTimes.add(e.get(5));
            durations.add(e.get(8));
            changes.add(e.get(9));
            trainIdentifiers.add(e.get(12));
        });

        return addElements(idSolutions,departureTimes,arrivalTimes,durations,changes,trainIdentifiers);
    }

    private static List<List<String>> addElements(List<String> idSolutions, List<String> departureTimes, List<String> arrivalTimes,List<String>durations,List<String> changes,List<String> trainIdentifiers){
        List<List<String>> tableOfTrains = new ArrayList<>();
        tableOfTrains.add(internFilter(idSolutions, "idsolution"));
        List<String> dt;
        dt = internFilter(departureTimes, "departuretime");
        tableOfTrains.add(time(dt));
        List<String> da;
        da = internFilter(arrivalTimes, "arrivaltime");
        tableOfTrains.add(time(da));
        tableOfTrains.add(internFilter(durations, "duration"));
        tableOfTrains.add(internFilter(changes, "changesno"));
        List<String> ti;
        ti = internFilter(trainIdentifiers, "trainlist");
        tableOfTrains.add(internFilter(ti, "trainidentifier"));
        return tableOfTrains;
    }

    private static List<String> internFilter(List<String> lis, String fElement){
        return lis.stream().filter(e -> e.contains(fElement))
                .map(e -> e.replace(fElement, ""))
                .map(e -> e.replace(":", ""))
                .map(e -> e.replace("\"", ""))
                .collect(Collectors.toList());
    }

    private static List<String> time(List<String> timeList){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS", Locale.US);
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("US/Central"));
        return timeList.stream()
                .map(e -> e.replace(e, sdf.format(calendar.getTime())))
                .collect(Collectors.toList());
    }
}
