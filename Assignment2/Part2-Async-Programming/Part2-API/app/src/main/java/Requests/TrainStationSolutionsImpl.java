package Requests;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static Utility.Functions.*;
import static Utility.UrlActions.*;

public class TrainStationSolutionsImpl implements TrainStationSolutions {

    private static final Logger LOG = Logger.getLogger(TrainStationSolutions.class.getName());
    private final static boolean DEBUG = false;

    @Override
    public JsonArray getTrainSolutions(String departureStation, String arrivalStation, String date, String time, String adultNumber, String childNumber) throws Exception {

        String url = buildUrlTrainSolutions(departureStation, arrivalStation, date, time, adultNumber, childNumber);
        String res = urlReader(url);
        if(DEBUG) LOG.log(Level.INFO,res);

        List<List<String>> filteredRes = applyFilter(res);
        if(DEBUG) printTableOfTrain(filteredRes);
        if(DEBUG) printTableOfTrainRaw(filteredRes);

        JsonArray ja = new JsonArray();

        IntStream.range(0, filteredRes.get(0).size()).boxed().forEach(e -> {
            String ob = "{\"idSolution\":\"" + filteredRes.get(0).get(e) + "\",\"departureTime\":\"" + filteredRes.get(1).get(e)
                    + "\",\"arrivalTime\":\"" + filteredRes.get(2).get(e) + "\",\"duration\":\"" + filteredRes.get(3).get(e)
                    + "\",\"changes\":\"" + filteredRes.get(4).get(e) + "\",\"trainIdentifier\":\"" + filteredRes.get(5).get(e) +"\"}";
            ja.add(new Gson().fromJson(ob, JsonObject.class));
        });
        if(DEBUG) LOG.log(Level.INFO, ja.toString());
        return ja;
    }

    @Override
    public JsonObject getRealTimeTrainInfo(String trainID) throws Exception {

        List<String> res = filterTrainInfo(buildUrlTrainInfo(trainID));
        if (DEBUG) LOG.log(Level.INFO, res.toString());

        String ob = "{\"ScheduledArrival\":\""+ res.get(0) + "\","
                + "\"EffectiveArrival\":\""+ res.get(1) + "\","
                + "\"ScheduledDeparture\":\""+ res.get(2) + "\","
                + "\"EffectiveArrival\":\""+ res.get(3) + "\"}";

        if(DEBUG) LOG.log(Level.INFO,ob);
        return new Gson().fromJson(ob, JsonObject.class);
    }

    @Override
    public JsonArray getRealTimeStationInfo(String stationID, String regione) throws Exception {

        List<String> res = filterStationInfo(buildUrlStationInfo(stationID,regione));
        if (DEBUG) LOG.log(Level.INFO, res.toString());

        List<List<String>> splitterList = splitStations(res);

        JsonArray ja = new JsonArray();

        IntStream.range(0, splitterList.get(0).size()).boxed().forEach(e -> {
            String ob = "{\"trainName\":\"" + splitterList.get(0).get(e) + "\",\"destination\":\"" + splitterList.get(1).get(e)
                    + "\",\"time\":\"" + splitterList.get(2).get(e) + "\",\"expectedBinary\":\"" + splitterList.get(3).get(e)
                    + "\",\"realBinary\":\"" + splitterList.get(4).get(e) + "\",\"delayTime\":\"" + splitterList.get(5).get(e) +"\"}";
            ja.add(new Gson().fromJson(ob, JsonObject.class));

        });
        if(DEBUG) ja.forEach(e -> LOG.log(Level.INFO, e.toString()));

        return ja;
    }

}
