package Requests;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public interface TrainStationSolutions {

    /**
     * Retrieves the list of possible travel solutions between a departure station and a destination station, on a specific date and starting at a certain time.
     * @param departureStation The name of the departure station
     * @param arrivalStation The name of the arrival station
     * @param date Date in dd / mm / yyyy format
     * @param time The departure time in hh format
     * @param adultNumber Number of adult passengers
     * @param childNumber Number of child passengers
     * @return JsonArray that contain data about top 5 train solutions
     * @throws Exception Connection
     */
    public JsonArray getTrainSolutions(String departureStation, String arrivalStation, String date, String time, String adultNumber, String childNumber) throws Exception;

    /**
     * Retrieves information about the current status of a specific train
     * @param trainID ID of train
     * @return JsonObject that contain data about train
     */
    public JsonObject getRealTimeTrainInfo(String trainID) throws Exception;

    /**
     * Retrieves information about the current status of a specific station
     * @param stationID ID of station
     * @param city Code of city where the station is located
     * @return JsonObject that contain data about train
     */
    public JsonArray getRealTimeStationInfo(String stationID, String city) throws Exception;
}
