package Requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrainList {

    @JsonProperty("trainidentifier")
    String trainidentifier;

    @JsonProperty("trainacronym")
    String trainacronym;

    @JsonProperty("traintype")
    String traintype;

    @JsonProperty("pricetype")
    String pricetype;

    public TrainList() {

    }

    @Override
    public String toString() {
        return "TrainList{" +
                "trainidentifier='" + trainidentifier + '\'' +
                '}';
    }
}
