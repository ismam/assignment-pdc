package Requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainSolution {

    @JsonProperty("idsolution")
    String idsolution;

    @JsonProperty("origin")
    String origin;

    @JsonProperty("destination")
    String destination;

    @JsonProperty("departuretime")
    String departuretime;

    @JsonProperty("arrivaltime")
    String arrivaltime;

    @JsonProperty("duration")
    String duration;

    @JsonProperty("changesno")
    String changesno;

    @JsonProperty("trainlist")
    ArrayList<TrainList> trainlist;


    public TrainSolution() {

    }

    @Override
    public String toString() {
        String train = " ";
        for(int i = 0 ; i < trainlist.size(); i ++) {
            train = new StringBuilder().append(train).append(",").append(trainlist.get(i).trainidentifier).toString();
        }

        return "{" +
                "\"idsolution\":\"" + idsolution + "\"," +
                "\"origin\":\"" + origin + "\"," +
                "\"destination\":\"" + destination + "\"," +
                "\"departuretime\":\"" + departuretime + "\"," +
                "\"arrivaltime\":\"" + arrivaltime + "\"," +
                "\"duration\":\"" + duration + "\"," +
                "\"changesno\":\"" + changesno + "\"," +
                "\"trainlist\":\"" + train.substring(2) + "\"" +
                "}";
    }
}
