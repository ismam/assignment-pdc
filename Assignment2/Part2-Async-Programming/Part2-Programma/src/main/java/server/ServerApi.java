/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import Requests.TrainStationSolutions;
import Requests.TrainStationSolutionsImpl;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

public class ServerApi {
    TrainStationSolutions tr;
    public ServerApi() {
        tr = new TrainStationSolutionsImpl();
    }

    public void getTrainSolutions(RoutingContext routingContext)  {
        String departureStation = routingContext.request().getParam("departureStation");
        String arrivalStation = routingContext.request().getParam("arrivalStation");
        String date = routingContext.request().getParam("date");
        String time = routingContext.request().getParam("time");
        String adultNumber = routingContext.request().getParam("adultNumber");
        String childNumber = routingContext.request().getParam("childNumber");

        JsonArray result = null;
        try {
            result = tr.getTrainSolutions(departureStation,arrivalStation,date,time,adultNumber,childNumber);
        } catch (Exception exception) {
            exception.printStackTrace();
            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .setStatusCode(500)
                    .end(Json.encodePrettily("Some error occurred, try again later"));
        }
        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .setStatusCode(200)
                .end(result.toString());
    }

    public void getRealTimeTrainInfo(RoutingContext routingContext) {
        String trainID = routingContext.request().getParam("trainID");
        JsonArray result = null;
        try {
            result = tr.getRealTimeTrainInfo(trainID);
        } catch (Exception exception) {
            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .setStatusCode(500)
                    .end(Json.encodePrettily("Some error occurred, try again later"));
        }
        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(result.toString()));
    }

    public void getRealTimeStationInfo(RoutingContext routingContext) {
        String stationID = routingContext.request().getParam("stationID");
        String regione = routingContext.request().getParam("regione");

        JsonArray result = null;
        try {
            result = tr.getRealTimeStationInfo(stationID,regione);
        } catch (Exception exception) {
            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .setStatusCode(500)
                    .end(Json.encodePrettily("Some error occurred, try again later"));
        }

        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(result.toString()));
    }
}
