/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.json.schema.SchemaParser;
import io.vertx.json.schema.SchemaRouter;
import io.vertx.json.schema.SchemaRouterOptions;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author hama
 */
public class Server extends AbstractVerticle {
    private Logger log = Logger.getLogger(Server.class.getName());
    private String host;
    private int port;

    public Server(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void start() throws Exception {
        log.log(Level.INFO, "Starting server");
        //Schema parser using for validation
        SchemaParser parser = SchemaParser.createDraft7SchemaParser(
                SchemaRouter.create(vertx, new SchemaRouterOptions())
        );
        Router router = Router.router(vertx);
        //Initialize Api class
        ServerApi api = new ServerApi();
        //Setup route
        ServerRoute routes = new ServerRoute(router, api, parser);

        //Start server and handle startup success and error
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(port,host)
                .onSuccess(server -> log.log(Level.INFO, "HTTP server start on {0}:{1}", new Object[]{host,server.actualPort()}))
                .onFailure(server -> log.log(Level.INFO, "Startup failed {0}" , server.getMessage()));

    }

    private void sendReply(RoutingContext request, JsonObject reply) {
        HttpServerResponse response = request.response();
        response.putHeader("content-type", "application/json");
        response.end(reply.toString());
    }
}
