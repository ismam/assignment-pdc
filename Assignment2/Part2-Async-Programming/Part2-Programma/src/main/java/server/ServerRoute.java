package server;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.validation.BadRequestException;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.json.schema.SchemaParser;

import static io.vertx.ext.web.validation.builder.Parameters.*;
import static io.vertx.json.schema.common.dsl.Schemas.intSchema;
import static io.vertx.json.schema.common.dsl.Schemas.stringSchema;


public class ServerRoute {

    private Router router;
    private ServerApi api;
    private SchemaParser parser;

    public ServerRoute(Router router, ServerApi api, SchemaParser parser) {
        this.router = router;
        this.api = api;
        this.parser = parser;
        setupDefaultRoute();
        setupRealTimeStationInfoRoute();
        setupTrainSolutionsRoute();
        setupTrainRealTimeInfoRoute();
        errorHandler();
    }

    //Setup default root
    private void setupDefaultRoute() {
        router.route(HttpMethod.GET, "/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            //Json for root request response
            JsonObject rootResponse = new JsonObject();
            rootResponse.put("List of available api", "");
            rootResponse.put("getTrainSolutions", "/api/trainsolution?departureStation=xx&arrivalStation=xx&date=dd/mm/yyyy&time=hh:MM:ss&adultNumber=x&childNumber=x");
            rootResponse.put("getRealTimeTrainInfo", "/api/trainrealtimeinfo?trainID=xx");
            rootResponse.put("getRTealTimeStationInfo(stationID, city)", "/api/trainstationinfo?stationID=xx&city=xx");
            response
                    .putHeader("content-type", "text/html")
                    .end(rootResponse.encodePrettily());
        });
    }

    /* Setup train solution route*/
    private void setupTrainSolutionsRoute() {
        ValidationHandler validationHandler = ValidationHandler.builder(parser)
                .queryParameter(param("departureStation", stringSchema()))
                .queryParameter(param("arrivalStation", stringSchema()))
                .queryParameter(param("date", stringSchema()))
                .queryParameter(param("time", stringSchema()))
                .queryParameter(param("adultNumber", intSchema()))
                .queryParameter(param("childNumber", intSchema()))
                .build();

        router.get("/api/trainsolution")
                .handler(validationHandler)
                .handler(routingContext -> {
                    api.getTrainSolutions(routingContext);
                });
    }


    /* Setup train real time info route*/
    private void setupTrainRealTimeInfoRoute() {
        ValidationHandler validationHandler = ValidationHandler.builder(parser)
                .queryParameter(param("trainID", stringSchema()))
                .build();
        router.route(HttpMethod.GET, "/api/trainrealtimeinfo")
                .handler(validationHandler)
                .handler(routingContext -> {
                    api.getRealTimeTrainInfo(routingContext);
                });
    }

    /* Setup train station info route*/
    private void setupRealTimeStationInfoRoute() {
        ValidationHandler validationHandler = ValidationHandler.builder(parser)
                .queryParameter(param("stationID", stringSchema()))
                .queryParameter(param("regione", stringSchema()))
                .build();

        router.route(HttpMethod.GET, "/api/trainstationinfo")
                .handler(validationHandler)
                .handler(routingContext -> {
                    api.getRealTimeStationInfo(routingContext);
                });
    }

    /* Handle some error */
    public void errorHandler() {
        router.errorHandler(400, routingContext -> {
            Throwable failure = routingContext.failure();
            if (failure instanceof BadRequestException) {
                String validationErrorMessage = failure.getMessage();
                routingContext.response().setStatusMessage(validationErrorMessage).setStatusCode(400).end();
            }
            if(failure instanceof NumberFormatException) {
                String validationErrorMessage = failure.getMessage();
                routingContext.response().setStatusMessage(validationErrorMessage).setStatusCode(400).end();
            }
        });
    }
}
