package client;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Gui {

    private JFrame frame;
    public JButton cerca1;
    public JButton cerca2;
    public JButton cerca3;
    public JLabel stazionePartenzaLabel;
    private JLabel stazioneArrivoLabel;
    private JLabel idTrenoLabel;
    private JLabel stazioneLabel;
    private JLabel dataLabel;
    private JLabel oraLabel;
    private JLabel numeroAdultiLabel;
    private JLabel numeroBambiniLabel;
    private JLabel regioneLabel;
    private JScrollPane scrollPane;
    public JTextField stazionePartenza;
    public JTextField stazioneArrivo;
    public JTextField idTreno;
    public JTextField stazione;
    public JTextField data;
    public JTextField ora;
    public JTextField numeroAdulti;
    public JTextField numeroBambini;
    public JTextField regione;
    public JTextArea txtArea;
    private JPanel panel;
    private JPanel topLeftPanel;
    private JPanel topCenterPanel;
    private JPanel topRightPanel;

    public void initializeGui(){
        this.panel = new JPanel();
        this.topCenterPanel = new JPanel();
        this.topRightPanel = new JPanel();
        this.topLeftPanel = new JPanel();
        frame = new JFrame("Second Assignment");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,700);

        cerca1 = new JButton("Cerca");
        cerca3 = new JButton("Cerca info");
        cerca2 = new JButton("Cerca info");

        txtArea = new JTextArea(30,50);
        stazionePartenza = new JTextField(15);
        stazioneArrivo = new JTextField(15);
        idTreno = new JTextField(15);
        stazione = new JTextField(15);
        data = new JTextField(15);
        ora = new JTextField(15);
        numeroAdulti = new JTextField(15);
        numeroBambini = new JTextField(15);
        regione = new JTextField(15);

        txtArea.setEditable(false);

        scrollPane = new JScrollPane(txtArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        stazionePartenzaLabel = new JLabel("Stazione di partenza");
        stazioneArrivoLabel = new JLabel("Stazione di arrivo");
        idTrenoLabel = new JLabel("Id treno");
        stazioneLabel = new JLabel("Stazione");
        dataLabel = new JLabel("Data");
        oraLabel = new JLabel("Orario");
        numeroAdultiLabel = new JLabel("Numero adulti");
        numeroBambiniLabel = new JLabel("Numero bambini");
        regioneLabel = new JLabel("Regione");

        panel.setBorder(new EmptyBorder(new Insets(30, 50, 100, 50)));

        topLeftPanel.setLayout(new BoxLayout(topLeftPanel, BoxLayout.Y_AXIS));
        topCenterPanel.setLayout(new BoxLayout(topCenterPanel, BoxLayout.Y_AXIS));
        topRightPanel.setLayout(new BoxLayout(topRightPanel, BoxLayout.Y_AXIS));

        topLeftPanel.add(dataLabel);
        topLeftPanel.add(data);

        topLeftPanel.add(oraLabel);
        topLeftPanel.add(ora);

        topLeftPanel.add(numeroAdultiLabel);
        topLeftPanel.add(numeroAdulti);

        topLeftPanel.add(numeroBambiniLabel);
        topLeftPanel.add(numeroBambini);

        topLeftPanel.add(regioneLabel);
        topLeftPanel.add(regione);

        topCenterPanel.add(stazionePartenzaLabel);
        topCenterPanel.add(stazionePartenza);

        topCenterPanel.add(stazioneArrivoLabel);
        topCenterPanel.add(stazioneArrivo);

        topCenterPanel.add(idTrenoLabel);
        topCenterPanel.add(idTreno);

        topCenterPanel.add(stazioneLabel);
        topCenterPanel.add(stazione);

        topRightPanel.add(Box.createRigidArea(new Dimension(0, 30)));
        topRightPanel.add(cerca1);
        topRightPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        topRightPanel.add(cerca2);
        topRightPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        topRightPanel.add(cerca3);

        panel.add(topLeftPanel);
        panel.add(topCenterPanel);
        panel.add(topRightPanel);


        panel.add(scrollPane);


        frame.getContentPane().add(panel);

        //make frame visible
        frame.setVisible(true);

    }
}
