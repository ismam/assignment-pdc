package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.ext.web.client.WebClient;

import java.util.List;
import java.util.Locale;


public class Controller {

    private Gui gui;
    private WebClient client;
    private String host;
    private String port;

    public Controller(WebClient client,String host, String port) {
        this.client = client;
        this.host = host;
        this.port = port;
    }

    public void initialize() {
        this.gui = new Gui();
        this.gui.initializeGui();
        gui.cerca1.addActionListener(e -> {
            String partenza = gui.stazionePartenza.getText().toUpperCase(Locale.ROOT);
            String arrivo = gui.stazioneArrivo.getText().toUpperCase(Locale.ROOT);
            String data = gui.data.getText();
            String ora = gui.ora.getText();
            String numeroAdulti = gui.numeroAdulti.getText();
            String numeroBambini = gui.numeroBambini.getText();

            client.get(Integer.parseInt(port),host,"/api/trainsolution?").
                    addQueryParam("departureStation",partenza).
                    addQueryParam("arrivalStation",arrivo).
                    addQueryParam("date",data).
                    addQueryParam("time",ora).
                    addQueryParam("adultNumber",numeroAdulti).
                    addQueryParam("childNumber",numeroBambini).
            send().onSuccess(re -> {
                String res = re.bodyAsString();
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    System.out.println("Risultato "  + res);
                    List<TrainSolution> trainSolutions = objectMapper.readValue(res, new TypeReference<>() {});

                    trainSolutions.forEach(ts -> gui.txtArea.append(ts.toString()));
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                    gui.txtArea.append("Response not valid");
                }
            });
        });
        gui.cerca2.addActionListener(e -> {
            String idTreno = gui.idTreno.getText();

            client.get(Integer.parseInt(port),host,"/api/trainrealtimeinfo?").
                    addQueryParam("trainID",idTreno).
                            send().onSuccess(re -> {
                String res = re.bodyAsString();
                ObjectMapper objectMapper = new ObjectMapper();
                try {

                    String serverResponse = res.replaceAll("\\\\", "");
                    serverResponse = serverResponse.substring(1, serverResponse.length() - 1);
                    List<TrainInformation> trainInformation = objectMapper.readValue(serverResponse, new TypeReference<>() {});

                    trainInformation.forEach(ts -> gui.txtArea.append(ts.toString()));
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                    gui.txtArea.append("Response not valid");
                }
            });
        });
        gui.cerca3.addActionListener(e -> {
            String stationId = gui.stazione.getText();
            String regione = gui.regione.getText();

            client.get(Integer.parseInt(port),host,"/api/trainstationinfo?").
                    addQueryParam("stationID",stationId).
                    addQueryParam("regione",regione).
                    send().onSuccess(re -> {
                String res = re.bodyAsString();
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    System.out.println("Risultato "  + res);
                    String serverResponse = res.replaceAll("\\\\", "");
                    serverResponse = serverResponse.substring(1, serverResponse.length() - 1);
                    List<TrainStationInfo> trainStationInfo = objectMapper.readValue(serverResponse, new TypeReference<>() {});

                    trainStationInfo.forEach(ts -> gui.txtArea.append(ts.toString()));
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                    gui.txtArea.append("Response not valid");
                }
            });
        });
    }
}
