package client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TrainStationInfo {

    @JsonProperty("trainName")
    String trainName;

    @JsonProperty("destination")
    String destination;

    @JsonProperty("time")
    String time;

    @JsonProperty("expectedBinary")
    String expectedBinary;

    @JsonProperty("realBinary")
    String realBinary;

    @JsonProperty("delayTime")
    String delayTime;

    @Override
    public String toString() {
        return "TrainStationInfo: \n" +
                "trainName=" + trainName + "\n" +
                "destination=" + destination + "\n" +
                "time=" + time + "\n" +
                "expectedBinary=" + expectedBinary + "\n" +
                "realBinary=" + realBinary + "\n" +
                "delayTime=" + delayTime + "\n\n\n\n";
    }
}
