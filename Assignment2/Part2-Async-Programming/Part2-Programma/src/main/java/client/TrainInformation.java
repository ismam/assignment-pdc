package client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainInformation {

    @JsonProperty("ScheduledArrival")
    String ScheduledArrival;

    @JsonProperty("EffectiveArrival")
    String EffectiveArrival;

    @JsonProperty("ScheduledDeparture")
    String ScheduledDeparture;

    @JsonProperty("EffectiveDeparture")
    String EffectiveDeparture;

    public TrainInformation() {

    }

    @Override
    public String toString() {
        return "Train information: \n" +
                ScheduledArrival + "\n" +
                EffectiveArrival + "\n" +
                ScheduledDeparture + "\n" +
                EffectiveDeparture + "\n";
    }
}
