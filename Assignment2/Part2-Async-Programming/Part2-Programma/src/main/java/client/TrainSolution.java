package client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainSolution {

    @JsonProperty("idsolution")
    String idsolution;

    @JsonProperty("origin")
    String origin;

    @JsonProperty("destination")
    String destination;

    @JsonProperty("departuretime")
    String departuretime;

    @JsonProperty("arrivaltime")
    String arrivaltime;

    @JsonProperty("duration")
    String duration;

    @JsonProperty("changesno")
    String changesno;

    @JsonProperty("trainlist")
    String trainlist;


    public TrainSolution() {

    }

    @Override
    public String toString() {
        return "Train solution: \n" +
                "idsolution:" + idsolution + "\n" +
                "origin:" + origin + "\n" +
                "destination:" + destination + "\n" +
                "departuretime:" + departuretime + "\n" +
                "arrivaltime:" + arrivaltime + "\n" +
                "duration:" + duration + "\n" +
                "changesno:" + changesno + "\n" +
                "trainlist:" + trainlist + "\n\n\n";
    }
}
