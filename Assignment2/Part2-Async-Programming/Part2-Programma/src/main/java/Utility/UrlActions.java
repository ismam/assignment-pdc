package Utility;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

public class UrlActions {

    public static String urlReader(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();

        }
    }

    public static String buildUrlTrainSolutions(String departureStation, String arrivalStation, String date, String time, String adultNumber, String childNumber) {
        String url = "https://www.lefrecce.it/msite/api/solutions?origin=";
        url = url + departureStation + "&destination=";                 // eg. MILANO%20CENTRALE
        url = url + arrivalStation + "&arflag=A&adate=";                // eg. ROMA%20TERMINI
        url = url + date + "&atime=";                                   // eg. 28/05/2019
        url = url + time + "&adultno=";                                 // eg. 17
        url = url + adultNumber + "&childno=";                          // eg. 1
        url = url + childNumber + "&direction=A&frecce=false&onlyRegional=false"; // eg. 0
        System.out.println("Url = " + url);
        return url;
    }

    public static List<String> buildUrlTrainInfo(String trainID) throws IOException {
        String url = "http://www.viaggiatreno.it/vt_pax_internet/mobile/scheda?numeroTreno="+trainID;
        Document doc = Jsoup.connect(url).get();
        return Arrays.asList(doc.select("p").get(2).toString(), doc.select("p").get(3).toString(), doc.select("p").get(0).toString(), doc.select("p").get(1).toString());
    }

    public static List<String> buildUrlStationInfo(String stationID, String city) throws Exception {

        Document doc = Jsoup.connect("http://www.viaggiatreno.it/vt_pax_internet/mobile/stazione?codiceStazione="+stationID+city)
                .data("codiceStazione", stationID+city) //hidden fields which are being passed in post request.
                .userAgent("Mozilla")
                .post();
        return Arrays.asList(doc.getElementsByClass("bloccorisultato").toString().split("<div class=\"bloccorisultato\"> ").clone());
    }

}