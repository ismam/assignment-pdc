package Utility;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Functions {

    public static List<String> filterTrainInfo(List<String> info){
        return info.stream().map(e -> e.replace("<p>","")
                .replace("<br>", "")
                .replace("<strong>", "")
                .replace("</strong>","")
                .replace("</p>",""))
                .collect(Collectors.toList());
    }

    public static List<String> filterStationInfo(List<String> info){
        return info.stream().map(e -> e.replace("<p>","")
                .replace("<br>", "")
                .replace("<strong>", "")
                .replace("</strong>","")
                .replace("</p>","")
                .replace("<h2>", "")
                .replace("</h2>","")
                .replace("<div class=\"bloccotreno\">", "")
                .replace("\"","")
                .replace("</div>", "")
                .replace("<a href=\"scheda?", "")
                .replace("lang=IT\">", "")
                .replace("Vedi scheda</a>", "")
                .replaceAll("\n[ \t]*\n", "\n"))
                .collect(Collectors.toList());
    }

    public static List<List<String>> splitStations(List<String> res){
        List<String> trainName = new ArrayList<>();
        List<String> destination = new ArrayList<>();
        List<String> time = new ArrayList<>();
        List<String> expectedBinary = new ArrayList<>();
        List<String> realBinary = new ArrayList<>();
        List<String> delayTime = new ArrayList<>();
        List<String> info = new ArrayList<>();

        List<List<String>> sublist = new ArrayList<>();

        res.forEach(e -> sublist.add(Arrays.asList(e.split("\n"))));
        sublist.remove(0);

        sublist.forEach(e -> {
            trainName.add(e.get(1));
            destination.add(e.get(3));
            time.add(e.get(5));
            expectedBinary.add(e.get(6));
            realBinary.add(e.get(7));
            delayTime.add(e.get(8));
            info.add(e.get(9));
        });

        List<List<String>> a = new ArrayList<>();
        a.add(trainName);
        a.add(destination);
        a.add(time);
        a.add(expectedBinary);
        a.add(realBinary);
        a.add(delayTime);
        a.add(info);
        return a;
    }

    private static List<String> time(List<String> timeList){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS", Locale.US);
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("US/Central"));
        return timeList.stream()
                .map(e -> e.replace(e, sdf.format(calendar.getTime())))
                .collect(Collectors.toList());
    }

    public static String fixWhiteSpace(String word) {
        String result = word.replaceAll("\\s", "%20");
        return result;
    }
}
