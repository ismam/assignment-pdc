import client.Controller;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import server.Server;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Logger log = Logger.getLogger(Main.class.getName());

        Vertx vertx = Vertx.vertx();
        //get host
        String host = System.getProperty("server.host", "localhost");
        //get port

        String port = System.getProperty("server.port", "8080");

        try {
            Server server = new Server(host, Integer.parseInt(port));
            vertx.deployVerticle(server);
        } catch (NumberFormatException ex) {
            log.log(Level.SEVERE, "Port not valid ");
        }

        //Start gui
        WebClient client = WebClient.create(vertx);
        Controller control = new Controller(client,host,port);
        control.initialize();

    }
}
