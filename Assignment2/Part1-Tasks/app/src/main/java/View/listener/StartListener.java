/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.listener;

import Controller.GuiController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class StartListener implements ActionListener {
    
    private final JTextField dirPath;
    private final JTextField exclusionFilePath;
    private final JTextField numberOfOccurrences;
    private String path;
    private String exclusion;
    private int occurrences;
    private final GuiController controller;
    private JButton exit;
    private JButton suspend;
    private JButton start;
    private JButton resumption;
    
    public StartListener (JTextField dirPath, JTextField exclusionFile, JTextField numberOfOccurrences, GuiController controller,JButton start ,JButton exit,JButton resumption ,JButton suspend) {
        this.dirPath = dirPath;
        this.exclusionFilePath = exclusionFile;
        this.numberOfOccurrences = numberOfOccurrences;
        this.controller = controller;
        this.exit = exit;
        this.suspend = suspend;
        this.start = start;
        this.resumption = resumption;
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {

        path = dirPath.getText().trim();
        exclusion = exclusionFilePath.getText().trim();
        occurrences = this.numberOfOccurrences.getText().isEmpty() || !this.numberOfOccurrences.getText().chars().allMatch(Character::isDigit) ?  -1 : Integer.parseInt(this.numberOfOccurrences.getText());

        if(path.isBlank() || !new File(path).isDirectory()) {
            JOptionPane.showMessageDialog(null, "Please enter a correct directory path");
            return;
        }

        if( !exclusion.isBlank() && !exclusion.endsWith(".txt") ){
            JOptionPane.showMessageDialog(null, "Please enter a correct exclusion file name");
            return;
        }

        if(occurrences < 0) {
            JOptionPane.showMessageDialog(null, "Please enter a correct number of occurrences");
            return;
        } else {
            try {
                 occurrences = Integer.parseInt(numberOfOccurrences.getText().trim());
            } catch (NumberFormatException e ) {
                JOptionPane.showMessageDialog(null, "Please enter a correct value");
                //We are on view, no need to print stacktrace
            }
        }
        
        new Thread(() -> {
            System.out.println("Start computation");
            exit.setEnabled(true);
            suspend.setEnabled(true);
            resumption.setEnabled(false);
            start.setEnabled(false);
            controller.setup(path, exclusion, occurrences);
            //Start compute
            List<String> result = controller.startWorking();
            if(result == null) {
                System.out.println("There is no result to display, Maybe something went wrong, please check your files");
            } else {
                result.forEach(System.out::println);
            }
            start.setEnabled(true);
        }).start();
    
    }
}
