package View.listener;

import Controller.GuiController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class StopListener implements ActionListener {

    GuiController controller;
    private JButton exit;
    private JButton suspend;
    private JButton start;
    private JButton resumption;

    public StopListener(GuiController controller,JButton start ,JButton exit,JButton resumption ,JButton suspend) {
        this.controller = controller;
        this.controller = controller;
        this.exit = exit;
        this.suspend = suspend;
        this.start = start;
        this.resumption = resumption;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        //Stop all computation
        new Thread(() -> {
            this.start.setEnabled(true);
            this.exit.setEnabled(false);
            this.resumption.setEnabled(false);
            this.suspend.setEnabled(false);
            controller.stop();
        }).start();
    }

}
