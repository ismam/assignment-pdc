/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.listener;

import Controller.GuiController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author hama
 */
public class ResumeListener implements ActionListener {

    private GuiController controller;
    private JButton exit;
    private JButton suspend;
    private JButton start;
    private JButton resumption;

    public ResumeListener(GuiController controller, JButton start ,JButton exit,JButton resumption ,JButton suspend) {
        this.controller = controller;
        this.controller = controller;
        this.exit = exit;
        this.suspend = suspend;
        this.start = start;
        this.resumption = resumption;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        new Thread(() -> {
            start.setEnabled(false);
            exit.setEnabled(true);
            suspend.setEnabled(true);
            resumption.setEnabled(false);
            controller.resume();
        }).start();
    }
}
