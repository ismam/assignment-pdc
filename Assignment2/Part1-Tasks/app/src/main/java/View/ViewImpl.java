package View;

import Controller.GuiController;
import Logger.CustomPrintStream;
import Logger.GuiOutPutStream;
import View.listener.ResumeListener;
import View.listener.StartListener;
import View.listener.StopListener;
import View.listener.SuspendListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.PrintStream;

public class ViewImpl implements View {

    private JFrame frame;
    private JButton start;
    private JButton suspend;
    private JButton exit;
    private JButton resumption;
    private JLabel pdfPath;
    private JLabel exclusionFile;
    private JLabel occurrence;
    private JScrollPane scrollPane;
    private JTextField dirPath;
    private JTextField exclusionFilePath;
    private JTextField numberOfOccurrences;
    private JTextArea txtArea;
    private JPanel panel;
    private GuiController controller;
    private GuiOutPutStream output;

    @Override
    public void prepareGui(GuiController controller) {
        this.controller = controller;
        this.panel = new JPanel();
        frame = new JFrame("First Assignment");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,700);

        start = new JButton("Start");
        exit = new JButton("Exit");
        suspend = new JButton("Suspend");
        resumption = new JButton("Resume");

        txtArea = new JTextArea(30,50);
        exclusionFilePath = new JTextField(15);
        numberOfOccurrences = new JTextField(15);
        dirPath = new JTextField(15);
        txtArea.setEditable(false);

        scrollPane = new JScrollPane(txtArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pdfPath = new JLabel("Insert directory that contain files");
        exclusionFile = new JLabel("Insert path of the exclusion file");
        occurrence = new JLabel("Insert number of occurrences");

        panel.setBorder(new EmptyBorder(new Insets(50, 50, 100, 50)));

        panel.add(exclusionFile);
        panel.add(exclusionFilePath);

        panel.add(occurrence);
        panel.add(numberOfOccurrences);

        panel.add(pdfPath);
        panel.add(dirPath);

        panel.add(scrollPane);

        panel.add(suspend);
        panel.add(exit);
        panel.add(start);
        panel.add(resumption);
        start.addActionListener(new StartListener(dirPath, exclusionFilePath, numberOfOccurrences, controller, start, exit, resumption,suspend));
        exit.addActionListener(new StopListener(controller, start, exit, resumption,suspend));
        suspend.addActionListener(new SuspendListener(controller, start, exit, resumption,suspend));
        resumption.addActionListener(new ResumeListener(controller, start, exit, resumption,suspend));
        exit.setEnabled(false);
        suspend.setEnabled(false);
        resumption.setEnabled(false);
        //Setup system out to print on textArea
        output = new GuiOutPutStream(txtArea);
        System.setOut(new PrintStream(output, true));
        System.setOut(new CustomPrintStream(System.out));
        
        frame.getContentPane().add(panel);
        
        //make frame visible 
        frame.setVisible(true);
    }
}
