package View;

import Controller.GuiController;

public interface View {
    void prepareGui(GuiController controller);
}
