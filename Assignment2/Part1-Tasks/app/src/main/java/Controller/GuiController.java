package Controller;

import Model.Document.DocumentService;
import Model.Document.DocumentServiceImpl;
import View.ViewImpl;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GuiController implements Controller {
    
    private static final Logger LOG = Logger.getLogger(GuiController.class.getName());
    private DocumentService documentService;
    private ExecutorService executor;
    private int poolSize;
    private String dirPath;
    private String exclusionFilePath;
    private int numberOfOccurrences;
    
    public void setup(String dirPath,String exclusionFilePath,int numberOfOccurrences) {
        //Calcule pool size
        poolSize = Runtime.getRuntime().availableProcessors() +1;
        //Initialize Executor
        executor = Executors.newFixedThreadPool(poolSize);
        documentService = new DocumentServiceImpl(executor);
        this.dirPath = dirPath;
        this.exclusionFilePath = exclusionFilePath;
        this.numberOfOccurrences = numberOfOccurrences;
          
    }
    
    @Override
    public void init() {
        new ViewImpl().prepareGui(this);
    }

    public List<String> startWorking() {
        try {
            //Execute compute
            return documentService.compute(dirPath, exclusionFilePath, numberOfOccurrences);
        } catch (IOException | InterruptedException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
            return null;
        }
    }
    
    @Override
    public void stop() {
        if(documentService != null) {
            documentService.stop();
        }
    }
    
    @Override
    public void resume() {
        if(documentService != null) {
            documentService.resume();
        }
    }
    
    @Override
    public void suspend() {
        if(documentService != null) {
            documentService.suspend();
        }
    } 
}
