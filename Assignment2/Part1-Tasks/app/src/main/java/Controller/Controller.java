package Controller;

public interface Controller {

    void init();
    void stop();
    void resume();
    void suspend();
    
}
