package Model.Document;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ComputeDocumentTask implements Runnable {

    private static final Logger LOG = Logger.getLogger(ComputeDocumentTask.class.getName());
    private final static boolean DEBUG = false;

    private final String filePath;
    private final List<String> excludedWords;
    private final DocumentResult documentResult;
    private volatile boolean suspended = false;
    private final Object o;
    private boolean taskCompleted = false;

    public ComputeDocumentTask(String filePath, List<String> excludedWords, DocumentResult documentResult) {
        this.filePath = filePath;
        this.excludedWords = excludedWords;
        this.documentResult = documentResult;
        this.o = new Object();
    }

    @Override
    public void run() {
        while (!taskCompleted) {
            if (!suspended) {
                if (DEBUG) {
                    LOG.log(Level.INFO, "Executing a Task");
                }
                try {
                    this.compute();
                } catch (IOException e) {
                    LOG.log(Level.SEVERE, e.getMessage());
                }
                if (DEBUG) {
                    LOG.log(Level.INFO, "Task ended, words is added to the map");
                }
            } else {
                try {
                    while (suspended) {
                        synchronized (o) {
                            System.out.println(Thread.currentThread().getName() + ": Current task is suspended, waiting to be resume");
                            o.wait();
                        }
                    }

                } catch (InterruptedException e) {
                    LOG.log(Level.SEVERE, "Something went wrong: {0}", e);
                    if (!Thread.currentThread().isInterrupted()) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }

    private void compute() throws IOException {
        try ( PDDocument document = PDDocument.load(new File(filePath))) {
            AccessPermission ap = document.getCurrentAccessPermission();
            checkAccessPermission(ap);
            count(document);
        }
    }

    private void count(PDDocument document) throws IOException {

        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);
        taskCompleted = false;
        for (int p = 1; p <= document.getNumberOfPages(); ++p) {
            stripper.setStartPage(p);
            stripper.setEndPage(p);
            String text = stripper.getText(document);

            text = text.toLowerCase();
            String[] words = text.trim().split("\\W+");
            List<String> myWords = Arrays.stream(words).collect(Collectors.toList());
            if (excludedWords == null) {
                this.addToMap(myWords.toArray(new String[0]));
            } else {
                this.addToMap(this.compareWords(this.excludedWords, myWords));
            }
        }
        taskCompleted = true;
    }

    private void addToMap(String[] words) {
        int totalWords = 0;
        for (String s : words) {
            documentResult.addMap(s);
            totalWords++;
        }
        documentResult.incrementWordCounter(totalWords);
    }

    private String[] compareWords(List<String> excludedWords, List<String> myWords) {

        excludedWords.forEach(v -> myWords.removeIf(s -> s.contains(v)));
        return myWords.toArray(new String[0]);
    }

    private void checkAccessPermission(AccessPermission ap) {
        if (!ap.canExtractContent()) {
            try {
                throw new IOException("You do not have permission to extract text");
            } catch (IOException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    public void suspend() {
        suspended = true;
    }

    public void resume() {
        suspended = false;
        synchronized (o) {
            o.notifyAll();
        }
    }
}
