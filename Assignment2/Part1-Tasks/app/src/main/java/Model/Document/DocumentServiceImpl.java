package Model.Document;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DocumentServiceImpl implements DocumentService{

    private static final Logger LOG = Logger.getLogger(DocumentServiceImpl.class.getName());
    private final static boolean DEBUG = true;

    private final ExecutorService executor;
    private List<ComputeDocumentTask> runningTask;
    private  List<String> excludedWords;

    public DocumentServiceImpl(ExecutorService executor){
        this.executor = executor;
        runningTask = new ArrayList<>();
    }


    @Override
    public List<String> compute(String dirPath,String exclusionFilePath,int numberOfOccurrences) throws IOException, InterruptedException {

        Long _start = System.currentTimeMillis();
        if(exclusionFilePath.isBlank()) {
            excludedWords = null;
        } else { 
            excludedWords = this.getExcludedWords(dirPath + File.separator + exclusionFilePath);
        }
        
        List<String> fileNames = getFileNames(dirPath + File.separator);

        DocumentResult documentResult = new DocumentResult(fileNames.size());
        fileNames.stream().map(filename -> new ComputeDocumentTask(filename, excludedWords, documentResult)).map(task -> {
            runningTask.add(task);
            return task;
        }).forEachOrdered(task -> {
            executor.execute(task);
        });
        
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        documentResult.updateResult(numberOfOccurrences);
        Long _stop = System.currentTimeMillis();
        Long _time = _stop - _start;
        System.out.println("End computation in " + _time + "ms");

        return documentResult.getResult();
    }

    private List<String> getFileNames(String dirPath){

        File folder = new File(dirPath);
        File[] listOfFiles = folder.listFiles();
        assert listOfFiles != null;
        return Arrays.stream(listOfFiles)
                .filter(File::isFile)
                .filter(v-> v.getName().endsWith("pdf"))
                .map(e -> dirPath + e.getName())
                .collect(Collectors.toList());
    }

    private List<String> getExcludedWords(String exclusionFilePath) throws IOException {

        String[] words;

        try (BufferedReader br = new BufferedReader(new FileReader(exclusionFilePath))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            String everything = sb.toString();
            everything = everything.toLowerCase();

            words = everything.split("\\r?\\n");
        } catch (FileNotFoundException e) {
            if (DEBUG) LOG.log(Level.INFO, "Exclusion file not found!");
            words = new String[]{"noFileFound"};
        }

        return Arrays.stream(words).collect(Collectors.toList());
    }

    @Override
    public void stop() {
        executor.shutdown();
        try {
            if(!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executor.shutdownNow();
            }
            System.out.println("Stopped all computation");
        } catch (InterruptedException e) {
            executor.shutdownNow();
            System.out.println("Stopped all computation");
        }
    }
    
    @Override
    public void suspend() {
        runningTask.forEach(task -> {
            task.suspend();
        });
    }
    
    @Override
    public void resume() {
        runningTask.forEach(task -> {
            task.resume();
        });
        System.out.println("Resume all task");
    }   
}
