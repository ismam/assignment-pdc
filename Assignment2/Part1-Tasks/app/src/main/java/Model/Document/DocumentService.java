package Model.Document;

import java.io.IOException;
import java.util.List;

public interface DocumentService {

    /**
     * From 3 parameters, get the most used n words
     * @param dirPath directory where all files is contained
     * @param exclusionFilePath path of the exclusion file
     * @param numberOfOccurrences number of word to be returned
     * @return most n used words
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    List<String> compute(String dirPath,String exclusionFilePath,int numberOfOccurrences) throws IOException, InterruptedException;
    void stop();
    void suspend();
    void resume();
}
