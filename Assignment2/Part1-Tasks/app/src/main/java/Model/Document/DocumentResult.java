package Model.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.IntStream;


public class DocumentResult {

    private static final Logger LOG = Logger.getLogger(DocumentResult.class.getName());
    private final static boolean DEBUG = true;

    private final Map<String, Integer> map;
    private final List<String> result;

    private final int nResultToWait;
    private int nResultArrived = 0;
    private int wordCounter = 0;


    public DocumentResult(int nTotalResult){
        this.nResultToWait = nTotalResult;
        map = new HashMap<>();
        result = new ArrayList<>();
    }

    public synchronized void addMap(String key){
        map.put(key, map.containsKey(key) ? map.get(key)+1 : 1);
        nResultArrived++;
        if(nResultToWait >= nResultArrived){
            notifyAll();
        }

    }

    public synchronized int getFileCounter(){ return this.nResultArrived; }

    public synchronized void incrementWordCounter(int numberOfWords){ this.wordCounter = this.wordCounter + numberOfWords; }

    public List<String> getResult() {
        return result;
    }

    public synchronized void updateResult(int occurrences){
        IntStream.range(0, occurrences+1).boxed().forEach(v -> {
            String key = "";
            int value = 0;
            for(Map.Entry<String, Integer> entry : map.entrySet()){
                if(entry.getValue()> value){
                    key = entry.getKey();
                    value = entry.getValue();
                }
            }
            result.add("The word '" + key + "' is used " + value + " times.");
            map.remove(key);
        });
    }

}
